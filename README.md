# LIS4381 - Mobile Web Applications

## Tyler Fatkin

*Course work:*

#### Assignment 1

[A1 README.md](a1/README.md "First Assignment")

    * Installed AMPPS, Java Compiler, and Android Studio
    * Defined git commands
    * Ran Bitbucket tutorials
    * Supplied screenshots of previously stated programs running
    * Supplied links to Bitbucket tutorials

#### Assigment 2

[A2 README.md](a2/README.md "Second Assignment")

    * Backwards engineered provided app "Healthy Recipies"
    * Created first app with multiple activities
    * Provided screenshots of the two app activities

#### Assignment 3

[A3 README.md](a3/README.md "Third Assignment")

    * Created an Entity Relationship Diagram based off business rules
    * Forward Engineered SQL statements for ERD
    * Provided Screenshot of ERD, SQL file, and MWB file

#### Project 1

[P1 README.md](p1/README.md "First Project")

    * Backwards engineered provided app "My Business Card"
    * Created second app with multiple activities
    * Learned to create a launcher icon for app and display it in activities
    * Learned to add borders, background color, and text shadow in Android Studio
    * Provided screenshots of the two app activities

#### Assignment 5

[A5 README.md](a5/README.md "Fifth Assignment")

    * Implemented database to a website
    * Implemented database input via website
    * Implemented database input error via server-side validation

#### Project 2

[P2 README.md](p2/README.md "Second Project")

    * Added Edit and Delete functionality to Assignment 5
    * Implemented database update error via server-side validation
    * Added photos to home page carousel
    * Added RSS Feed to home page