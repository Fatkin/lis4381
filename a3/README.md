# LIS4381 - Mobile Web Applications

## Tyler Fatkin

### Assignment 3 Requirements:

1. Create a database from provided business rules
2. Screenshot, SQL file, and MySQL Workbench files of the database
3. Chapter Questions (Chs 5, 6)

#### Assignment Screenshots:

*Screenshot of ERD:*

![A3 ERD Screenshot](img/A3.png "ERD")

*SQL File:*

[A3 SQL File](A3.sql "SQL File")

*MWB File:*

[A3 Workbench File](A3.mwb "MWB File")