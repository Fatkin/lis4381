# LIS4381 - Mobile Web Applications

## Tyler Fatkin

### Assignment 5 Requirements:

1. Implement database in website
2. Implement server-side validation for data input
3. Chapter Questions

#### Assignment Screenshots:

*Screenshot of Database:*

![A5 Database Screenshot](img/a5_db.png "Database")

*Screenshot of Database Input:*

![A5 Database Screenshot](img/a5_db_input.png "Database")

*Screenshot of Server-side Validation:*

![A5 Database Screenshot](img/a5_db_error.png "Database")