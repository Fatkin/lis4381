> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Applications

## Tyler Fatkin

### Assignment 1 Requirements:

*Three Parts:*

1. Distriuted Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPpS installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands with short description
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creats a new local repository
2. git status - Lists changes and those that still need to be committed
3. git add - adds a file to staging index
4. git commit - commits any added files
5. git push - sends commits/changes to the repository
6. git pull - retrieves changes from the repository to the local machine
7. git clone - copies a full repository to the local machine

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png "AMPPS")

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png "Java Hello")

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png "Android Studio")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Fatkin/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Fatkin/myteamquotes/ "My Team Quotes Tutorial")
