# LIS4381 - Mobile Web Applications

## Tyler Fatkin

### Assignment 2 Requirements:

1. Backwards engineering of a provided application
2. Screenshot of app activities
3. Chapter Questions (Chs 3, 4)

#### Assignment Screenshots:

*Screenshot of app first activity:*

![First Activity](img/A2_1.png "Screen 1")

*Screenshot of app second activity:*

![Second Activity](img/A2_2.png "Screen 2")