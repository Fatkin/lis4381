# LIS4381 - Mobile Web Applications

## Tyler Fatkin

### Project 2 Requirements:

1. Add "Edit" and "Delete" functionality to [previous assignment](a5/README.md)
2. Added RSS Feed to home page of localhost
3. Chapter Questions

#### Assignment Screenshots:

*Screenshot of database:*

![P2 Database Screenshot](img/P2_1.png "Database")

*Screenshot of Database Edit with previously entered values:*

![P2 Database Update Screenshot](img/P2_2.png "Database Update")

*Screenshot of Edit Process Validation:*

![P2 Validation Screenshot](img/P2_3.png "Database Update Error")

*Screenshot of Carousel on Home Page:*

![P2 Carousel Screenshot](img/P2_Carousel.png "Carousel")

*Screenshot of RSS Feed:*

![P2 RSS Feed Screenshot](img/P2_RSS.png "RSS Feed")

