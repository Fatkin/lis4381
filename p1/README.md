# LIS4381 - Mobile Web Applications

## Tyler Fatkin

### Project 1 Requirements:

1. Backwards engineering of a provided application
2. Screenshot of app activities
3. Chapter Questions (Chs 7, 8)

#### Assignment Screenshots:

*Screenshot of app first activity:*

![First Activity](img/P1_1.png "Screen 1")

*Screenshot of app second activity:*

![Second Activity](img/P1_2.png "Screen 2")